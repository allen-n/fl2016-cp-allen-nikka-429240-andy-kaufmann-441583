# We are going to use EaselJS and PHP-Larvel frameworks to complete this project. #

1. **Dr. Drew’s City Escape (100 pts):**
2. **Pencil Avatar (15 pts)**
1. Avatar moves with arrow keys/WASD (7 pts)
2. Avatar can be aesthetically customized by user (8 pts)
3. **Pencil Drawing Ability (15 pts)**
1. Pencil can draw shapes from a predetermined list (5 pts)
2. Shapes drawn become a part of the environment. (10 pts)
4. **Gameplay (35 pts)**
1. User is able to play the level after drawing to reach end token (10pts)
2. Game difficulty elements (time, number shapes, platform placement) (15  pts)
3. Implements timer (5 pts)
4. Implements number of shapes (5 pts)
5. Implements preplaced platforms with increasing difficulty level (5pts) 
5. **Game Enemy (10 pts)**
1. User is killed upon contact (5pts)
2. Police Chief Rhombus placement with increasing difficulty level (5pts)
6. **Scores are recorded in a user database (10 Points)**
1. User receives score at end of level (5pts)
2. Score is saved based off of username at end of gameplay(5pts)
7. **Creative (20pts)**
1. Some ideas:
2. Giving police chief rhombus basic AI
3. Multiplayer mode
4. Different gameplay modes (Speed rounds, platforming sans drawn in shapes, etc.)
5. Sharing scores to social media
6. Custom shapes

8. **On time Rubric (5 pts)**















503S Project


The two experiments that we are going to run are comparing Node.js and Apache+PHP comparison on the same AWS instance type and PHP performance with and without caching. 
This will allow us to evaluate the potential performance of a multiplayer version of our game using Node.js as opposed to apache, 
as well as determine how our game (which is using a Laravel php and easelJS back end) performs with and without caching.